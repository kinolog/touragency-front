import {Injectable} from '@angular/core';
import { Http } from '@angular/http';
import { AuthenticationService } from './authentication.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { OrderEntity } from '../entities/order';
import {UserEntity} from '../entities/user';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';

@Injectable()
export class OrdersService {

  constructor( private http: Http,
    private authenticationService: AuthenticationService,
    private httpClient: HttpClient) {
  }

	getCurrentUser() {
		// return this.httpClient.get<UserEntity>('http://localhost:8080/home/profile');
		return this.authenticationService.getCurrentUser();
	}

	logout() {
		this.authenticationService.logout();
	}

	getOrdersForUser(user: UserEntity) {
      return this.http.get('http://localhost:8080/orders/byUserId/' + user.userId)
           .pipe(map(response =>
			   {console.log(response); return response.json(); }),
           catchError((error: any) => throwError(error.json().error || 'Server error')));
    }

}
