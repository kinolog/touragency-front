import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import {AuthenticationService} from '../services/authentication.service';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    username: string;
	password: string;
	loading = false;
	error = '';
	redirectUrl: string;

   constructor(private router: Router,
			  private authenticationService: AuthenticationService) { }

	ngOnInit(): void {
		this.authenticationService.logout();
	}

	login() {
		this.loading = true;
		console.log(this.username);
		console.log(this.password);
		let result = this.authenticationService.login(this.username, this.password)
			.subscribe(
				result => {
					console.log(result);
					this.authenticationService.setCurrentUser(result);
					if (result) {
						console.log('home');
						this.router.navigate(['home']);
					} else {
						this.error = 'Username or password is incorrect';
						this.loading = false;
					}
				},
				error => {
					this.error = error;
					this.loading = false;
				}
			);
	}

}
