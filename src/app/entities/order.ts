import {UserEntity} from './user';
import {TourEntity} from './tour';
import DateTimeFormat = Intl.DateTimeFormat;

export class OrderEntity {
    // constructor(
    //    public orderId?: number,
    //    public userId?: number,
    //    public tourId?: number
     // ) { }
    orderId?: number;
    user?: UserEntity;
    tour?: TourEntity;
    isConfirmed?: boolean;
    timeKey?: Date;

     constructor(order?: any) {
        if (order != null) {
            this.orderId = order.orderId;
            this.tour = order.tour;
            this.user = order.user;
            this.isConfirmed = order.confirmed;
            this.timeKey = new Date(order.timeKey.year, order.timeKey.monthValue, order.timeKey.dayOfMonth,
                order.timeKey.hour, order.timeKey.minute, order.timeKey.second);
        }
     }
}
