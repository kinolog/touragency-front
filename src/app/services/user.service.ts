import {Injectable} from '@angular/core';
import { Http } from '@angular/http';
import { AuthenticationService } from './authentication.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import {toPromise} from 'rxjs/operators';

import { UserEntity } from '../entities/user';

@Injectable()
export class UserService {

  constructor( private http: Http,
    private authenticationService: AuthenticationService,
    private httpClient: HttpClient) {
  }

	getCurrentUser() {
		//return this.httpClient.get<UserEntity>('http://localhost:8080/home/profile');
		return this.authenticationService.getCurrentUser();
	}

	getUserByEmail(email: string) {
		return this.httpClient.get<UserEntity>('http://localhost:8080/users/byEmail/' + email);
	}

	logout() {
		this.authenticationService.logout();
	}

}
