import { Component } from '@angular/core';
import {Router} from '@angular/router';
import { ChangeDetectorRef } from '@angular/core';

import {AuthenticationService} from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

	constructor(private router: Router, private authService: AuthenticationService, private cdRef:ChangeDetectorRef) {

	}

	ngAfterViewChecked() { 
    	this.cdRef.detectChanges();
  	}

  	logout() {
    	this.authService.logout();
    	this.router.navigate(['/']);
	}
}
