import {OrderEntity} from './order';

export class UserEntity {
    constructor(
        public userId?: number,
        public firstName?: string,
        public lastName?: string,
		public email?: string,
		public password?: string,
        public ordersList?: OrderEntity[]
     ) { }
}
