import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Http, HttpModule} from '@angular/http';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { LoginComponent } from './login/login.component';
import { CoreModule } from './core/core.module';
//import { HomeComponent } from './home/home.component';
import { HomeModule } from './home/home.module';

import {AppRoutingModule} from './app-routing.module';
//import { UserComponent } from './user/user.component';

import {UserService} from './services/user.service';
import {TOKEN_NAME} from './services/auth.constant';
import {AuthenticationService} from './services/authentication.service';
import {AuthGuard} from './guards/auth-guard.service';

@NgModule({
  declarations: [
    AppComponent,
	WelcomeComponent,
	LoginComponent,
//	HomeComponent,
//	UserComponent
  ],
  imports: [
    BrowserModule,
	HttpModule,
	CoreModule,
	AppRoutingModule,
	FormsModule,
    HomeModule
  ],
  providers: [ AuthenticationService,
    UserService,
	AuthGuard ],
  bootstrap: [AppComponent]
})
export class AppModule { }
