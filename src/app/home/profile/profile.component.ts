import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import { UserEntity } from '../../entities/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
	user: UserEntity;

	constructor(private router: Router, private userService: UserService) {}

	ngOnInit() {
		this.user = this.userService.getCurrentUser();
		console.log('user');
		console.log(this.user);
	}

	logout() {
		this.userService.logout();
		this.router.navigate(['welcome']);
   }
}
