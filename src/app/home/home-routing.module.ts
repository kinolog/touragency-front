import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { HomeComponent } from './home.component';
import { ProfileComponent } from './profile/profile.component';
import { OrdersComponent } from './orders/orders.component';
import { AuthGuard } from '../guards/auth-guard.service';

const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'home/profile', component: ProfileComponent},
  { path: 'home/orders', component: OrdersComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {
}
