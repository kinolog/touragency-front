import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Http, HttpModule} from '@angular/http';

import { CoreModule } from '../core/core.module';

import { HomeComponent } from './home.component';

import {HomeRoutingModule} from './home-routing.module';
//import { UserComponent } from '../user/user.component';
import { ProfileComponent } from './profile/profile.component';
import { OrdersComponent } from './orders/orders.component';

import {UserService} from '../services/user.service';
import {OrdersService} from '../services/orders.service';
import {AuthenticationService} from '../services/authentication.service';
import {AuthGuard} from '../guards/auth-guard.service';

@NgModule({
  declarations: [
	HomeComponent,
	//UserComponent,
    ProfileComponent,
	OrdersComponent
  ],
  imports: [
    BrowserModule,
	HttpModule,
	HomeRoutingModule,
	FormsModule,
    CoreModule
  ],
  providers: [ AuthenticationService,
    UserService,
      OrdersService,
	AuthGuard ],
  bootstrap: [HomeComponent]
})
export class HomeModule { }
