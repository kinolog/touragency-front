export class TourEntity {
    constructor(
        public tourId?: number,
        public tourName?: string,
        public tourDescription?: string,
		public location?: string,
		public startDate?: string,
		public endDate?: string,
		public countLimit?: number
     ) { }
}
