import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {Router, ActivatedRoute} from '@angular/router';
import { OrderEntity} from '../../entities/order';
import { OrdersService } from '../../services/orders.service';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

    orders: OrderEntity[];

    constructor(private router: Router, private authenticationService: AuthenticationService,
                private ordersService: OrdersService,
                private userService: UserService) { }

    ngOnInit() {
	    console.log('orders works');
        this.ordersService.getOrdersForUser(this.authenticationService.getCurrentUser()).subscribe((orders: OrderEntity[]) => {
            this.orders = orders.map((o) => new OrderEntity(o));
            console.log(orders);
            console.log(this.orders);
        } );
        console.log(this.orders);
    }

    logout() {
        this.userService.logout();
        this.router.navigate(['welcome']);
    }

}
