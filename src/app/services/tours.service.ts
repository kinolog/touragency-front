import {Injectable} from '@angular/core';
import { Http } from '@angular/http';
import { AuthenticationService } from './authentication.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import {toPromise} from 'rxjs/operators';

import { TourEntity } from '../entities/tour';

@Injectable()
export class ToursService {

  constructor( private http: Http,
    private authenticationService: AuthenticationService,
    private httpClient: HttpClient) {
  }

	getCurrentUser() {
		// return this.httpClient.get<UserEntity>('http://localhost:8080/home/profile');
		return this.authenticationService.getCurrentUser();
	}

	logout() {
		this.authenticationService.logout();
	}

}
