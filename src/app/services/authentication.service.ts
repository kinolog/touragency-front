import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import {map, catchError} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';

import { UserEntity } from '../entities/user';

@Injectable()
export class AuthenticationService {
  private authUrl = 'http://localhost:8080/perform_login';
  private logoutUrl = 'http://localhost:8080/logout';
  private headers = new Headers({'Content-Type': 'application/json'});
  private currentUser: UserEntity;

  constructor(private http: Http) {
  }

  login(email: string, password: string) {
	let user = new UserEntity(0, '', '', email, password);
   
	return this.http.post(this.authUrl, user).pipe(map(response => response.json()),
				catchError((error: any) => throwError(error.json().error || 'Server error')));
  }

	isLoggedIn(): boolean {
  		return this.currentUser !== null;
	}

	getCurrentUser(): UserEntity {
      return this.currentUser;
    }

	setCurrentUser(user: UserEntity) {
		this.currentUser = user;
	}
 
    logout(): void {
        // clear token remove user from local storage to log user out
        this.currentUser = null;
		this.http.post(this.logoutUrl, {});
    }
}
